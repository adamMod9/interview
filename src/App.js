import React, { Component } from 'react';
import Transcript from './transcript'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to Remeeting</h1>
        </header>
        <Transcript secret='YHM6x2NjgtP97NWB2A8zXGQd' recognitionId='6bfc' baseUrl='https://api-dev.remeeting.com' />
      </div>
    );
  }
}

export default App;
